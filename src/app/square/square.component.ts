import { Component, HostBinding, OnInit} from '@angular/core';
import {GameServiceService} from '.././services/game-service.service';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.scss']
})

export class SquareComponent implements OnInit {
  @HostBinding('class.flex-container')
  squaresAmount: number = 100;
  squaresArr = [];
  winClass: string = "flex-item"
  
 
  constructor(private gameservice : GameServiceService) { 
    for (let i = 0; i <= this.squaresAmount-1; i++) {
      this.squaresArr.push({"index": i})
    }
  }

  check() {
  debugger;
    
    if(this.gameservice.clickAllowed) {
      console.log("win")
    } else {
      console.log("loose")
    }
  }

  ngOnInit(): void {

  }

}
