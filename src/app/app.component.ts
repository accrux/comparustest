import { Component, ViewChildren, QueryList, ElementRef, AfterViewInit } from '@angular/core';
import { SquareComponent } from './square/square.component';
import {GameServiceService} from './services/game-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements AfterViewInit {
  @ViewChildren(SquareComponent, { read: ElementRef }) squares: QueryList<ElementRef>

  constructor(private myService :GameServiceService) { 
  }
  
  ngAfterViewInit() {
   this.myService.startGame(this.squares)
  }
}
